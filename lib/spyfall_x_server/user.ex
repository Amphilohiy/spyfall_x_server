defmodule SpyfallXServer.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias SpyfallXServer.User
  use Coherence.Schema


  schema "users" do
    field :email, :string
    field :name, :string
    coherence_schema()

    timestamps()
  end

  @doc false
  def changeset(%User{} = user, attrs) do
    user
    |> cast(attrs, [:name, :email] ++ coherence_fields())
    |> validate_required([:name, :email])
    |> validate_format(:email, ~r/@/)
    |> unique_constraint(:name)
    |> unique_constraint(:email)
    |> validate_coherence(attrs)
  end

  def changeset(%User{} = user, attrs, :password) do
    user
    |> cast(attrs, ~w(password password_confirmation reset_password_token reset_password_sent_at))
    |> validate_coherence_password_reset(attrs)
  end
end
