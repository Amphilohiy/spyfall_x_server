defmodule SpyfallXServerWeb.Router do
  use SpyfallXServerWeb, :router
  use Coherence.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session  # Add this
  end

  pipeline :protected do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug Coherence.Authentication.Session, protected: true  # Add this
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/" do
    pipe_through :browser
    coherence_routes()
  end

  scope "/", SpyfallXServerWeb do
    pipe_through :browser
    get "/", PageController, :index
  end

  # Add this block
  scope "/" do
    pipe_through :protected
    coherence_routes :protected
  end


  scope "/", SpyfallXServerWeb do
    pipe_through :protected
    # Add protected routes below
  end

  # Other scopes may use custom stacks.
  # scope "/api", SpyfallXServerWeb do
  #   pipe_through :api
  # end
end
