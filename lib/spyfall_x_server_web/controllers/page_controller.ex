defmodule SpyfallXServerWeb.PageController do
  use SpyfallXServerWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
