# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :spyfall_x_server,
  ecto_repos: [SpyfallXServer.Repo]

# Configures the endpoint
config :spyfall_x_server, SpyfallXServerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "+p1J6gNFh8/B0RzPMow2klMWDeoLBgFA6jMOjGp6pOlmdhUvr+J+7wkZ+39DCe4x",
  render_errors: [view: SpyfallXServerWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: SpyfallXServer.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configures templates engines
config :phoenix, :template_engines,
  pug: PhoenixExpug.Engine

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"

# %% Coherence Configuration %%   Don't remove this line
config :coherence,
  user_schema: SpyfallXServer.User,
  repo: SpyfallXServer.Repo,
  module: SpyfallXServer,
  web_module: SpyfallXServerWeb,
  router: SpyfallXServerWeb.Router,
  messages_backend: SpyfallXServerWeb.Coherence.Messages,
  logged_out_url: "/",
  email_from_name: "Your Name",
  email_from_email: "yourname@example.com",
  opts: [:authenticatable, :recoverable, :lockable, :trackable, :unlockable_with_token, :invitable, :registerable]

config :coherence, SpyfallXServerWeb.Coherence.Mailer,
  adapter: Swoosh.Adapters.Sendgrid,
  api_key: "your api key here"
# %% End Coherence Configuration %%
